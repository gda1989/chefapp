package mytest.chefapp.enums;

import android.util.SparseArray;

import org.jetbrains.annotations.NotNull;

public enum ListSortModesEnum {

    byDate(0, "by date"),
    byName(1, "by title");

    private static SparseArray<ListSortModesEnum> mapping;

    private final Integer key;
    private final String title;

    ListSortModesEnum(Integer key, String title) {
        this.key = key;
        this.title = title;
    }

    public static ListSortModesEnum getMode(Integer integer) {
        if (mapping == null) initMapping();
        return mapping.get(integer);
    }

    private static void initMapping() {
        mapping = new SparseArray<>();
        for (ListSortModesEnum e : values()) {
            mapping.put(e.key, e);
        }
    }

    public Integer getKey() {
        return key;
    }

    public String getTitle() {
        return title;
    }

    @NotNull
    @Override
    public String toString() {
        return title;
    }

}
