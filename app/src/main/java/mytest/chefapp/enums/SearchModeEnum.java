package mytest.chefapp.enums;

import android.util.SparseArray;

public enum SearchModeEnum {

    byTitle(1, "title"),
    byDescription(2, "description"),
    byInstructions(3, "instructions");

    private static SparseArray<SearchModeEnum> mapping;
    private final Integer key;
    private final String value;

    SearchModeEnum(Integer key, String value) {
        this.key = key;
        this.value = value;
    }

    public static SearchModeEnum getMode(Integer integer) {
        if (mapping == null) initMapping();
        return mapping.get(integer);
    }

    private static void initMapping() {
        mapping = new SparseArray<>();
        for (SearchModeEnum e : values()) {
            mapping.put(e.key, e);
        }
    }

    public Integer getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

}
