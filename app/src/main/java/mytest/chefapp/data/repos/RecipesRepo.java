package mytest.chefapp.data.repos;

import io.reactivex.Single;
import mytest.chefapp.network.network_models.RecipeDetailsResponse.RecipeDetailsResponse;
import mytest.chefapp.network.network_models.RecipeListResponse.RecipesResponse;

public interface RecipesRepo {

    Single<RecipesResponse> getRecipesList();

    Single<RecipeDetailsResponse> getRecipeDetails(String recipeUuid);

}
