package mytest.chefapp.data.repos.impl;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import mytest.chefapp.data.repos.RecipesRepo;
import mytest.chefapp.network.ApiClient;
import mytest.chefapp.network.network_models.RecipeDetailsResponse.RecipeDetailsResponse;
import mytest.chefapp.network.network_models.RecipeListResponse.RecipesResponse;

public class RecipesRepoImpl implements RecipesRepo {
    @Override
    public Single<RecipesResponse> getRecipesList() {
        return ApiClient.getInstance().getRecipes()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<RecipeDetailsResponse> getRecipeDetails(String recipeUuid) {
        return ApiClient.getInstance().getRecipeDetails(recipeUuid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
