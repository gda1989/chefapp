package mytest.chefapp.data.interactors;

import android.annotation.SuppressLint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import mytest.chefapp.data.repos.RecipesRepo;
import mytest.chefapp.enums.SearchModeEnum;
import mytest.chefapp.network.network_models.RecipeDetailsResponse.RecipeDetailsModel;
import mytest.chefapp.network.network_models.RecipeListResponse.Recipe;
import mytest.chefapp.ui.models.RecipeBriefViewModel;

public class RecipesInteractor {

    private final RecipesRepo recipesRepo;

    private RecipesInteractor(RecipesRepo recipesRepo) {
        this.recipesRepo = recipesRepo;
    }

    private static RecipesInteractor instance;

    public static RecipesInteractor getInstance(RecipesRepo recipesRepo) {
        if (instance == null)
            instance = new RecipesInteractor(recipesRepo);
        return instance;
    }

    private HashMap<String, RecipeDetailsModel> recipesFullInfoCache = new HashMap<>();
    private HashMap<String, RecipeBriefViewModel> briefsMap = new HashMap<>();

    public Single<List<RecipeBriefViewModel>> getRecipes() {
        return recipesRepo.getRecipesList().map(recipesResponse -> {
            List<RecipeBriefViewModel> result = new ArrayList<>();
            for (Recipe recipe : recipesResponse.getRecipes()) {
                RecipeBriefViewModel model = new RecipeBriefViewModel(recipe.getUuid(), recipe.getName(), recipe.getImages().get(0), recipe.getDescription(), recipe.getLastUpdated(), recipe.getInstructions());
                result.add(model);
                briefsMap.put(model.getUuid(), model);
            }
            return result;
        })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<RecipeDetailsModel> getRecipeDetails(String recipeUuid) {
        if (recipesFullInfoCache.containsKey(recipeUuid) && recipesFullInfoCache.get(recipeUuid) != null) {
            return new Single<RecipeDetailsModel>() {
                @Override
                protected void subscribeActual(SingleObserver<? super RecipeDetailsModel> observer) {
                    observer.onSuccess(recipesFullInfoCache.get(recipeUuid));
                }
            };
        } else {
            return recipesRepo.getRecipeDetails(recipeUuid).map(recipeDetailsResponse -> {
                recipesFullInfoCache.put(recipeUuid, recipeDetailsResponse.getRecipe());
                RecipeBriefViewModel model = new RecipeBriefViewModel(recipeDetailsResponse.getRecipe().getUuid(),
                        recipeDetailsResponse.getRecipe().getName(), recipeDetailsResponse.getRecipe().getImages().get(0),
                        recipeDetailsResponse.getRecipe().getDescription(), recipeDetailsResponse.getRecipe().getLastUpdated(), recipeDetailsResponse.getRecipe().getInstructions());
                briefsMap.put(model.getUuid(), model);
                return recipeDetailsResponse.getRecipe();
            })
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread());
        }
    }

    @SuppressLint("CheckResult")
    public Single<List<RecipeBriefViewModel>> performSearch(String search, int mode) {
        List<RecipeBriefViewModel> result = new ArrayList<>();
        return new Single<List<RecipeBriefViewModel>>() {
            @Override
            protected void subscribeActual(SingleObserver<? super List<RecipeBriefViewModel>> observer) {
                for (String key : briefsMap.keySet()) {
                    RecipeBriefViewModel model = briefsMap.get(key);
                    switch (SearchModeEnum.getMode(mode)) {
                        case byTitle:
                            if (model.getDescription() != null && model.getName().toLowerCase().contains(search.toLowerCase()))
                                result.add(briefsMap.get(key));
                            break;
                        case byDescription:
                            if (model.getDescription() != null && model.getDescription().toLowerCase().contains(search.toLowerCase()))
                                result.add(briefsMap.get(key));
                            break;
                        case byInstructions:
                            if (model.getDescription() != null && model.getInstructions().toLowerCase().contains(search.toLowerCase()))
                                result.add(briefsMap.get(key));
                    }
                }
                observer.onSuccess(result);
            }
        }
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
