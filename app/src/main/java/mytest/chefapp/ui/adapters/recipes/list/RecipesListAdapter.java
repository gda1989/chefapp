package mytest.chefapp.ui.adapters.recipes.list;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Collections;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import mytest.chefapp.R;
import mytest.chefapp.enums.ListSortModesEnum;
import mytest.chefapp.ui.adapters.recipes.OnRecipeClickListener;
import mytest.chefapp.ui.models.RecipeBriefViewModel;

public class RecipesListAdapter extends RecyclerView.Adapter<RecipesListItemViewHolder> {

    private List<RecipeBriefViewModel> briefViewModels;
    private OnRecipeClickListener listener;

    public RecipesListAdapter(List<RecipeBriefViewModel> briefViewModels, OnRecipeClickListener listener) {
        this.briefViewModels = briefViewModels;
        this.listener = listener;
    }

    public void updateList(List<RecipeBriefViewModel> briefViewModels) {
        this.briefViewModels = briefViewModels;
        notifyDataSetChanged();
    }

    @SuppressLint("CheckResult")
    public void sortList(int sortMode) {
        Completable.fromRunnable(() -> Collections.sort(briefViewModels, (o1, o2) -> {
            switch (ListSortModesEnum.getMode(sortMode)) {
                case byDate:
                    return o1.getTimestamp().compareTo(o2.getTimestamp());
                case byName:
                    return o1.getName().compareTo(o2.getName());
                default:
                    return o1.getUuid().compareTo(o2.getUuid());
            }
        }))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::notifyDataSetChanged);
    }

    @NonNull
    @Override
    public RecipesListItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recipes_list, parent, false);
        return new RecipesListItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecipesListItemViewHolder holder, int position) {
        holder.bind(briefViewModels.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return briefViewModels == null ? 0 : briefViewModels.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
