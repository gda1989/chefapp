package mytest.chefapp.ui.adapters.recipes.similar_recipes;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import mytest.chefapp.R;
import mytest.chefapp.network.network_models.RecipeDetailsResponse.RecipeDetailsModel;
import mytest.chefapp.ui.adapters.recipes.OnRecipeClickListener;

class SimilarViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.similar_item_image)
    ImageView image;
    @BindView(R.id.similar_item_name)
    TextView name;
    @BindView(R.id.similar_item_parent)
    LinearLayout parent;

    SimilarViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    void bind(RecipeDetailsModel model, OnRecipeClickListener listener){
        Glide.with(itemView.getContext())
                .load(model.getImages().get(0))
                .apply(new RequestOptions().centerCrop())
                .into(image);
        name.setText(model.getName());
        parent.setOnClickListener(v -> listener.onRecipeClick(model.getUuid()));
    }

}
