package mytest.chefapp.ui.adapters.recipes.similar_recipes;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import mytest.chefapp.R;
import mytest.chefapp.network.network_models.RecipeDetailsResponse.RecipeDetailsModel;
import mytest.chefapp.ui.adapters.recipes.OnRecipeClickListener;


public class SimilarAdapter extends RecyclerView.Adapter<SimilarViewHolder> {

    private OnRecipeClickListener listener;
    private List<RecipeDetailsModel> models;

    public SimilarAdapter(OnRecipeClickListener listener, List<RecipeDetailsModel> models) {
        this.listener = listener;
        this.models = models;
    }

    public void updateList(List<RecipeDetailsModel> models){
        this.models = models;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public SimilarViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_similar_recipe, parent, false);
        return new SimilarViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    @Override
    public void onBindViewHolder(@NonNull SimilarViewHolder holder, int position) {
        holder.bind(models.get(position), listener);
    }

    @Override
    public long getItemId(int position){
        return position;
    }
}
