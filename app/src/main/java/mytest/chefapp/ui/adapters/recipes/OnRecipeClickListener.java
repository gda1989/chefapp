package mytest.chefapp.ui.adapters.recipes;

public interface OnRecipeClickListener {

    void onRecipeClick(String uuid);

}
