package mytest.chefapp.ui.adapters.recipes.list;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import mytest.chefapp.R;
import mytest.chefapp.ui.adapters.recipes.OnRecipeClickListener;
import mytest.chefapp.ui.models.RecipeBriefViewModel;

public class RecipesListItemViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.item_recipes_list_image)
    ImageView image;
    @BindView(R.id.item_recipes_list_name)
    TextView name;
    @BindView(R.id.item_recipes_list_description)
    TextView description;
    @BindView(R.id.item_recipes_list_parent)
    ConstraintLayout parent;


    RecipesListItemViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    void bind(RecipeBriefViewModel model, OnRecipeClickListener listener){
        Glide.with(itemView.getContext())
                .load(model.getImageLink())
                .apply(new RequestOptions().centerCrop())
                .into(image);
        name.setText(model.getName());
        description.setText(model.getDescription());
        parent.setOnClickListener(v -> listener.onRecipeClick(model.getUuid()));
    }
}
