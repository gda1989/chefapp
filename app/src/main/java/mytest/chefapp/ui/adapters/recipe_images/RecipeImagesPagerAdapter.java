package mytest.chefapp.ui.adapters.recipe_images;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import mytest.chefapp.R;
import mytest.chefapp.ui.main.fragments.details.OnImageClickListener;

public class RecipeImagesPagerAdapter extends PagerAdapter {

    private Context context;
    private List<String> imageLinks = new ArrayList<>();
    private OnImageClickListener listener;

    public RecipeImagesPagerAdapter(Context context, List<String> imageLinks, OnImageClickListener listener) {
        this.context = context;
        this.imageLinks = imageLinks;
        this.listener = listener;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        LayoutInflater inflater = LayoutInflater.from(context);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.item_recipe_image_frame, collection, false);
        bindImage(imageLinks.get(position), layout);
        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "image_" + position;
    }


    @Override
    public int getCount() {
        return imageLinks.size();
    }

    private void bindImage(String link, View view) {
        ImageView image = view.findViewById(R.id.recipe_image);
        Glide.with(view)
                .load(link)
                .into(image);
        image.setOnClickListener(v -> {
            listener.onImageClick(link);
        });
    }


}
