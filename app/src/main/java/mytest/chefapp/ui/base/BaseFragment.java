package mytest.chefapp.ui.base;

import androidx.fragment.app.Fragment;

import butterknife.Unbinder;

public class BaseFragment extends Fragment {

    protected Unbinder unbinder;

    private String title;

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitile() {
        return title;
    }

    @Override
    public void onDestroy() {
        if (unbinder != null) unbinder.unbind();
        super.onDestroy();
    }

}
