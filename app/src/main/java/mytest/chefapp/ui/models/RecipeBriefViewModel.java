package mytest.chefapp.ui.models;

public class RecipeBriefViewModel {

    private final String uuid;
    private final String name;
    private final String imageLink;
    private final String description;
    private final Long timestamp;
    private final String instructions;

    public String getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public String getImageLink() {
        return imageLink;
    }

    public String getDescription() {
        return description;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public String getInstructions() {
        return instructions;
    }

    public RecipeBriefViewModel(String uuid, String name, String imageLink, String description, Long timestamp, String instructions) {
        this.uuid = uuid;
        this.name = name;
        this.imageLink = imageLink;
        this.description = description;
        this.timestamp = timestamp;
        this.instructions = instructions;
    }

}
