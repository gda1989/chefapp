package mytest.chefapp.ui.main.fragments.details;

public interface OnImageClickListener {

    void onImageClick(String link);

}
