package mytest.chefapp.ui.main;

import java.util.List;

import mytest.chefapp.network.network_models.RecipeDetailsResponse.Similar;

public interface MainViewInterface {

    void onRecipeClick(String uuid);

    void getSimilarList(List<Similar> similars);

    void onImageClick(String imageLink);

    void onSearchClick(String search, int mode);

    void onSaveImageClick(String link);

}
