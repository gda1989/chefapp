package mytest.chefapp.ui.main;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import moxy.presenter.InjectPresenter;
import mytest.chefapp.R;
import mytest.chefapp.eventbus.ConnectionEvent;
import mytest.chefapp.eventbus.DownloadResultEvent;
import mytest.chefapp.eventbus.RetryConnectionEvent;
import mytest.chefapp.network.network_models.RecipeDetailsResponse.Similar;
import mytest.chefapp.presenters.MainPresenter;
import mytest.chefapp.ui.base.BaseFragment;
import mytest.chefapp.ui.base.moxy.MvpAppCompatActivity;
import mytest.chefapp.ui.main.fragments.BigImageFragment;
import mytest.chefapp.ui.main.fragments.details.RecipeDetailsFragment;
import mytest.chefapp.ui.main.fragments.RecipesListFragment;
import mytest.chefapp.utils.InternetConnectionChecker;

public class MainActivity extends MvpAppCompatActivity implements MainMvpInterface, MainViewInterface {

    private static final int ASK_WRITE_STORAGE_PERMISSION_CODE = 159;

    @BindView(R.id.fragment_box)
    FrameLayout fragmentBox;
    @BindView(R.id.image_fragment_box)
    FrameLayout bigImageFragmentBox;
    @BindView(R.id.main_no_internet_frame)
    FrameLayout noInternetFrame;

    @InjectPresenter
    MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        hideKeyboard();
        setNoInternetFrameVisibility(!InternetConnectionChecker.getInstance().connected());
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void onEvent(DownloadResultEvent event) {
        Toast.makeText(this, event.getResult(), Toast.LENGTH_SHORT).show();
    }

    private void hideKeyboard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        View view = getCurrentFocus();
        if (view == null) view = new View(this);
        ((InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void onEvent(ConnectionEvent event) {
        setNoInternetFrameVisibility(event.isHaveNetworkProblems());
    }

    private void setNoInternetFrameVisibility(boolean visible) {
        if (noInternetFrame.getTranslationY() == 0 ^ visible) {
            noInternetFrame.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
            noInternetFrame.animate().translationY(visible ? 0 : -200).setDuration(1000);
        }
    }

    private void replaceFragment(BaseFragment fragment, boolean createNew) {
        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = false;
        if (!createNew) {
            fragmentPopped = manager.popBackStackImmediate(fragment.getTitile(), 0);
            if (fragmentPopped)
                fragment.onResume();
        }
        if (!fragmentPopped) {
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(fragmentBox.getId(), fragment);
            transaction.addToBackStack(fragment.getTitile());
            transaction.commitAllowingStateLoss();
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            if (bigImageFragmentBox.getVisibility() == View.VISIBLE) {
                presenter.onBigImageCloseClick();
                return;
            }
            super.onBackPressed();
        } else finish();
    }

    @Override
    public void onRecipeClick(String uuid) {
        presenter.onRecipeClick(uuid);
    }

    @Override
    public void getSimilarList(List<Similar> similars) {
        presenter.getSimilarRecipes(similars);
    }

    @Override
    public void onImageClick(String imageLink) {
        presenter.onImageClick(imageLink);
    }

    @Override
    public void onSearchClick(String search, int mode) {
        presenter.performSearch(search, mode);
    }

    @Override
    public void onSaveImageClick(String link) {
        presenter.onSaveImageClick(link);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        if (requestCode == ASK_WRITE_STORAGE_PERMISSION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                presenter.saveImage();
            }
        }
    }

    @Override
    public void showRecipesListFragment() {
        replaceFragment(RecipesListFragment.getInstance(), false);
    }

    @Override
    public void showRecipeInfoFragment(String uuid) {
        replaceFragment(RecipeDetailsFragment.getInstance(uuid), true);
    }

    @Override
    public void showBigImageFragment(String imageLink) {
        bigImageFragmentBox.setVisibility(View.VISIBLE);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(bigImageFragmentBox.getId(), BigImageFragment.getInstance(imageLink));
        transaction.commitAllowingStateLoss();
    }

    @Override
    public void hideBigImageFragment() {
        bigImageFragmentBox.setVisibility(View.GONE);
    }

    @Override
    public void askPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, ASK_WRITE_STORAGE_PERMISSION_CODE);
        } else {
            presenter.saveImage();
        }
    }

    @Override
    public void showGlobalError() {
        Toast.makeText(this, getString(R.string.global_error_text), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUnknownHostException() {
        showErrorSnackBar();
    }

    @Override
    public void onConnectException() {
        showErrorSnackBar();
    }

    public void showErrorSnackBar() {
        Snackbar.make(fragmentBox, "Data loading failed. Try again?", BaseTransientBottomBar.LENGTH_INDEFINITE)
                .setAction("Retry.", v -> EventBus.getDefault().post(new RetryConnectionEvent()))
                .show();
    }
}