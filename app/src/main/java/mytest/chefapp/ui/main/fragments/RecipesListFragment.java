package mytest.chefapp.ui.main.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import mytest.chefapp.ChefApp;
import mytest.chefapp.R;
import mytest.chefapp.enums.ListSortModesEnum;
import mytest.chefapp.enums.SearchModeEnum;
import mytest.chefapp.ui.adapters.recipes.OnRecipeClickListener;
import mytest.chefapp.ui.adapters.recipes.list.RecipesListAdapter;
import mytest.chefapp.ui.base.BaseFragment;
import mytest.chefapp.ui.live_data.LiveDataManager;
import mytest.chefapp.ui.main.MainViewInterface;
import mytest.chefapp.ui.models.RecipeBriefViewModel;

public class RecipesListFragment extends BaseFragment implements OnRecipeClickListener {

    public static RecipesListFragment getInstance() {
        return new RecipesListFragment();
    }

    public RecipesListFragment() {
        setTitle("recipes_list");
    }

    @BindView(R.id.list_header_input)
    EditText searchInput;
    @BindView(R.id.list_header_chevron)
    ImageView searchChevron;
    @BindView(R.id.list_header_search_icon)
    ImageView searchIcon;
    @BindView(R.id.list_header_constraint)
    ConstraintLayout searchParent;
    @BindView(R.id.content_list_recycler)
    RecyclerView recipesRecyclerView;
    @BindView(R.id.search_radio_group)
    RadioGroup searchRadioGroup;
    @BindView(R.id.radio_button_by_title)
    RadioButton rbByTitle;
    @BindView(R.id.radio_button_by_description)
    RadioButton rbByDescription;
    @BindView(R.id.radio_button_by_instructions)
    RadioButton rbByInstrucutions;
    @BindView(R.id.content_list_header_spinner)
    AppCompatSpinner spinner;

    @Inject
    LiveDataManager liveDataManager;

    private RecipesListAdapter adapter;
    private LiveData<List<RecipeBriefViewModel>> liveBriefs;
    LiveData<HashMap<String, List<RecipeBriefViewModel>>> liveSearchResults;
    private List<RecipeBriefViewModel> briefViewModels;

    boolean searchExpanded;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        unbinder = ButterKnife.bind(this, view);
        ChefApp.get().getComponent().inject(this);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initLiveData();
        initSearchElements();
        initSortModeSpinner();
    }

    @Override
    public void onResume() {
        super.onResume();
//        adapter = null;
    }

    private void initSortModeSpinner() {
        if (getContext() != null) {
            ListSortModesEnum[] modes = {ListSortModesEnum.byDate, ListSortModesEnum.byName};
            spinner.setAdapter(new ArrayAdapter<>(getContext(), R.layout.item_dropdown_sort_spinner, R.id.spinner_sort_mode_item_text, modes));
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    ListSortModesEnum sortMode = ListSortModesEnum.getMode(position);
                    if (adapter != null)
                        adapter.sortList(sortMode.getKey());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    private void initLiveData() {
        liveBriefs = liveDataManager.getLiveBriefs();
        liveBriefs.observe(this, listToShow -> {
            RecipesListFragment.this.briefViewModels = listToShow;
            initRecyclerView(RecipesListFragment.this.briefViewModels);
        });
    }

    private void initRecyclerView(List<RecipeBriefViewModel> listToShow) {
        if (adapter == null || recipesRecyclerView.getAdapter() == null) {
            adapter = new RecipesListAdapter(briefViewModels, this);
            LinearLayoutManager manager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
            adapter.setHasStableIds(true);
            recipesRecyclerView.setAdapter(adapter);
            recipesRecyclerView.setLayoutManager(manager);
        } else adapter.updateList(listToShow);
    }

    private void initSearchElements() {
        rbByTitle.setChecked(true);
        searchChevron.setOnClickListener(v -> {
            if (searchExpanded)
                performHideSearchMenu();
            else performShowSearchMenu();
        });
        searchIcon.setOnClickListener(v -> {
            String search = searchInput.getText().toString();
            if (getActivity() != null && !search.isEmpty()) {
                int mode = 0;
                if (searchRadioGroup.getCheckedRadioButtonId() == rbByTitle.getId())
                    mode = SearchModeEnum.byTitle.getKey();
                else if (searchRadioGroup.getCheckedRadioButtonId() == rbByDescription.getId())
                    mode = SearchModeEnum.byDescription.getKey();
                else if (searchRadioGroup.getCheckedRadioButtonId() == rbByInstrucutions.getId())
                    mode = SearchModeEnum.byInstructions.getKey();
                ((MainViewInterface) getActivity()).onSearchClick(search, mode);
            }
            liveSearchResults = liveDataManager.getSearchResults();
            liveSearchResults.observe(RecipesListFragment.this, stringListHashMap -> {
                if (stringListHashMap.containsKey(search)) {
                    initRecyclerView(stringListHashMap.get(search));
                    liveSearchResults.removeObservers(RecipesListFragment.this);
                }
            });
        });
        searchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!searchInput.getText().toString().isEmpty() && !searchExpanded) {
                    performShowSearchMenu();
                } else if (searchInput.getText().toString().isEmpty() && searchExpanded) {
                    performHideSearchMenu();
                }
                if (searchInput.getText().toString().isEmpty())
                    initRecyclerView(briefViewModels);
            }
        });
    }

    private void performShowSearchMenu() {
        searchExpanded = true;
        searchChevron.animate().rotation(-90);
        searchRadioGroup.setVisibility(View.VISIBLE);
        TransitionManager.beginDelayedTransition(searchParent);
    }

    private void performHideSearchMenu() {
        searchExpanded = false;
        searchChevron.animate().rotation(90);
        searchRadioGroup.setVisibility(View.GONE);
        TransitionManager.beginDelayedTransition(searchParent);
    }

    @Override
    public void onDestroy() {
        if (liveBriefs != null)
            liveBriefs.removeObservers(this);
        super.onDestroy();
    }


    @Override
    public void onRecipeClick(String uuid) {
        if (getActivity() != null) {
            ((MainViewInterface) getActivity()).onRecipeClick(uuid);
        }
    }
}
