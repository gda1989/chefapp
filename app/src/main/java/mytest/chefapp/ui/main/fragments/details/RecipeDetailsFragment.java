package mytest.chefapp.ui.main.fragments.details;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import me.relex.circleindicator.CircleIndicator;
import mytest.chefapp.ChefApp;
import mytest.chefapp.R;
import mytest.chefapp.network.network_models.RecipeDetailsResponse.RecipeDetailsModel;
import mytest.chefapp.network.network_models.RecipeDetailsResponse.Similar;
import mytest.chefapp.ui.adapters.recipe_images.RecipeImagesPagerAdapter;
import mytest.chefapp.ui.adapters.recipes.OnRecipeClickListener;
import mytest.chefapp.ui.adapters.recipes.similar_recipes.SimilarAdapter;
import mytest.chefapp.ui.base.BaseFragment;
import mytest.chefapp.ui.elements.DifficultyElement;
import mytest.chefapp.ui.live_data.LiveDataManager;
import mytest.chefapp.ui.main.MainViewInterface;

public class RecipeDetailsFragment extends BaseFragment implements OnRecipeClickListener, OnImageClickListener {

    private static final String UUID_KEY = "uuid_key";

    @BindView(R.id.recipe_details_name)
    TextView name;
    @BindView(R.id.recipe_details_description)
    TextView description;
    @BindView(R.id.recipe_details_images_viewpager)
    ViewPager imagesViewPager;
    @BindView(R.id.recipe_details_images_circle_indicator)
    CircleIndicator circleIndicator;
    @BindView(R.id.recipe_details_recipe_text)
    TextView recipeText;
    @BindView(R.id.recipe_details_similar_header_text)
    TextView similarHeader;
    @BindView(R.id.recipe_details_similar_line)
    View similarLine;
    @BindView(R.id.recipe_details_similar_recyclerview)
    RecyclerView similarRecyclerView;
    @BindView(R.id.recipe_details_difficulty)
    DifficultyElement difficultyElement;

    public static RecipeDetailsFragment getInstance(String uuid) {
        return new RecipeDetailsFragment(uuid);
    }

    private RecipeDetailsFragment(String uuid) {
        setTitle(uuid);
    }

    public RecipeDetailsFragment() {
    }

    @Inject
    LiveDataManager liveDataManager;

    private LiveData<HashMap<String, RecipeDetailsModel>> liveRecipes;
    private RecipeDetailsModel recipeDetailsModel;

    private SimilarAdapter similarAdapter;

    boolean infoShown;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recipe_details, container, false);
        unbinder = ButterKnife.bind(this, view);
        ChefApp.get().getComponent().inject(this);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey(UUID_KEY))
            setTitle(savedInstanceState.getString(UUID_KEY));
    }

    @Override
    public void onResume() {
        super.onResume();
        infoShown = false;
        initLiveData();
    }

    private void initLiveData() {
        liveRecipes = liveDataManager.getLiveRecipeDetails();
        liveRecipes.observe(this, stringRecipeDetailsModelHashMap -> {
            if (stringRecipeDetailsModelHashMap.containsKey(getTitile()) && stringRecipeDetailsModelHashMap.get(getTitile()) != null) {
                if (!infoShown) {
                    recipeDetailsModel = stringRecipeDetailsModelHashMap.get(getTitile());
                    setData();
                    if (getActivity() != null && !recipeDetailsModel.getSimilar().isEmpty()) {
                        ((MainViewInterface) getActivity()).getSimilarList(recipeDetailsModel.getSimilar());
                    } else {
                        similarHeader.setVisibility(View.GONE);
                        similarLine.setVisibility(View.GONE);
                    }
                } else {
                    initSimilarRecycler(stringRecipeDetailsModelHashMap);
                }
            }
        });
    }

    private void setData() {
        infoShown = true;
        if (recipeDetailsModel != null) {
            if (recipeDetailsModel.getDifficulty() != null){
                difficultyElement.setVisibility(View.VISIBLE);
                difficultyElement.setDifficulty(recipeDetailsModel.getDifficulty());}
            name.setText(recipeDetailsModel.getName());
            description.setText(recipeDetailsModel.getDescription());
            recipeText.setText(Html.fromHtml(recipeDetailsModel.getInstructions()));
            initViewPager();
        }
    }

    private void initViewPager() {
        if (recipeDetailsModel != null) {
            RecipeImagesPagerAdapter recipeImagesPagerAdapter = new RecipeImagesPagerAdapter(getContext(), recipeDetailsModel.getImages(), this);
            imagesViewPager.setAdapter(recipeImagesPagerAdapter);
            circleIndicator.setViewPager(imagesViewPager);
        }
    }

    @SuppressLint("CheckResult")
    private void initSimilarRecycler(HashMap<String, RecipeDetailsModel> modelHashMap) {
        List<RecipeDetailsModel> similarsDetails = new ArrayList<>();
        Completable.fromRunnable(() -> {
            for (Similar similar : recipeDetailsModel.getSimilar()) {
                if (modelHashMap.containsKey(similar.getUuid()) && modelHashMap.get(similar.getUuid()) != null) {
                    similarsDetails.add(modelHashMap.get(similar.getUuid()));
                }
            }
        })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    if (similarAdapter == null || similarRecyclerView.getAdapter() == null) {
                        similarAdapter = new SimilarAdapter(this, similarsDetails);
                        LinearLayoutManager manager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
                        similarAdapter.setHasStableIds(true);
                        similarRecyclerView.setAdapter(similarAdapter);
                        similarRecyclerView.setLayoutManager(manager);
                    } else {
                        similarAdapter.updateList(similarsDetails);
                    }
                });
    }

    @Override
    public void onSaveInstanceState(@NotNull Bundle outstate) {
        super.onSaveInstanceState(outstate);
        outstate.putString(UUID_KEY, getTitile());
    }

    @Override
    public void onDestroy() {
        liveRecipes.removeObservers(this);
        super.onDestroy();
    }

    @Override
    public void onRecipeClick(String uuid) {
        if (getActivity() != null) {
            ((MainViewInterface) getActivity()).onRecipeClick(uuid);
        }
    }

    @Override
    public void onImageClick(String link) {
        if (getActivity() != null) {
            ((MainViewInterface) getActivity()).onImageClick(link);
        }
    }
}
