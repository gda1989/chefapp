package mytest.chefapp.ui.main;

import moxy.MvpView;
import moxy.viewstate.strategy.AddToEndSingleStrategy;
import moxy.viewstate.strategy.OneExecutionStateStrategy;
import moxy.viewstate.strategy.StateStrategyType;

public interface MainMvpInterface extends MvpView {

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showRecipesListFragment();

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showRecipeInfoFragment(String uuid);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void showBigImageFragment(String imageLink);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void hideBigImageFragment();

    @StateStrategyType(OneExecutionStateStrategy.class)
    void askPermission();

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showGlobalError();

    @StateStrategyType(OneExecutionStateStrategy.class)
    void onUnknownHostException();

    @StateStrategyType(OneExecutionStateStrategy.class)
    void onConnectException();
}
