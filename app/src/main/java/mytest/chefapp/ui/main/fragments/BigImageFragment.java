package mytest.chefapp.ui.main.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import mytest.chefapp.R;
import mytest.chefapp.ui.base.BaseFragment;
import mytest.chefapp.ui.main.MainViewInterface;

public class BigImageFragment extends BaseFragment {

    private static final String IMAGELINK_KEY = "imagelink_key";

    public static BigImageFragment getInstance(String imageLink) {
        BigImageFragment fragment = new BigImageFragment();
        Bundle args = new Bundle();
        args.putString(IMAGELINK_KEY, imageLink);
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.big_image)
    ImageView bigImage;
    @BindView(R.id.big_dots)
    ImageView dots;

    private String link;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image_big, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null && getArguments().containsKey(IMAGELINK_KEY)) {
            link = getArguments().getString(IMAGELINK_KEY);
            Glide.with(this)
                    .load(link)
                    .into(bigImage);
        }
        initElements();
    }

    private void initElements(){
        dots.setOnClickListener(v -> {
            showPopUpMenu();
        });
    }

    private void showPopUpMenu(){
        PopupMenu popupMenu = new PopupMenu(getContext(), dots);
        popupMenu.inflate(R.menu.popupmenu);
        popupMenu.setOnMenuItemClickListener(item -> {
            if(getActivity() != null){
                ((MainViewInterface) getActivity()).onSaveImageClick(link);
            }
            return true;
        });
        popupMenu.show();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
