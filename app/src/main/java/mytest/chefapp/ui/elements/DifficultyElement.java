package mytest.chefapp.ui.elements;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;
import mytest.chefapp.R;

public class DifficultyElement extends LinearLayout {

    @BindView(R.id.difficulty_cap_1)
    ImageView cap1;
    @BindView(R.id.difficulty_cap_2)
    ImageView cap2;
    @BindView(R.id.difficulty_cap_3)
    ImageView cap3;
    @BindView(R.id.difficulty_cap_4)
    ImageView cap4;
    @BindView(R.id.difficulty_cap_5)
    ImageView cap5;


    public DifficultyElement(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        View view = LayoutInflater.from(context).inflate(R.layout.element_difficulty, null);
        ButterKnife.bind(this, view);
        this.addView(view);
    }

    public void setDifficulty(int level){
        int color;
        if(level < 3) color = getContext().getResources().getColor(R.color.difficulty_easy);
        else if(level > 4) color = getContext().getResources().getColor(R.color.difficulty_hard);
        else color = getContext().getResources().getColor(R.color.difficulty_medium);
        ImageView[] imageViews = {cap1, cap2, cap3, cap4, cap5};
        for(int i = 0; i < level; i++){
            imageViews[i].setColorFilter(color);
        }
    }

}
