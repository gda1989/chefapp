package mytest.chefapp.ui.live_data;

import androidx.lifecycle.MutableLiveData;

import java.util.HashMap;
import java.util.List;

import mytest.chefapp.network.network_models.RecipeDetailsResponse.RecipeDetailsModel;
import mytest.chefapp.ui.models.RecipeBriefViewModel;

public class LiveDataManager {

    private static LiveDataManager instance;

    public static LiveDataManager getInstance() {
        if (instance == null) instance = new LiveDataManager();
        return instance;
    }

    //briefs
    MutableLiveData<List<RecipeBriefViewModel>> liveBriefs = new MutableLiveData<>();

    public MutableLiveData<List<RecipeBriefViewModel>> getLiveBriefs() {
        return liveBriefs;
    }

    public void updateBriefs(List<RecipeBriefViewModel> briefViewModels) {
        liveBriefs.postValue(briefViewModels);
    }

    //recipeDetails
    HashMap<String, RecipeDetailsModel> detailsMap = new HashMap<>();
    MutableLiveData<HashMap<String, RecipeDetailsModel>> liveRecipeDetails = new MutableLiveData<>();

    public MutableLiveData<HashMap<String, RecipeDetailsModel>> getLiveRecipeDetails() {
        return liveRecipeDetails;
    }

    public void addRecipeDetails(String uuid, RecipeDetailsModel detailsModel) {
        detailsMap.put(uuid, detailsModel);
        liveRecipeDetails.postValue(detailsMap);
    }

    //searchResults
    MutableLiveData<HashMap<String, List<RecipeBriefViewModel>>> liveSearchResults = new MutableLiveData<>();

    public MutableLiveData<HashMap<String, List<RecipeBriefViewModel>>> getSearchResults() {
        return liveSearchResults;
    }

    public void setSearchResults(String search, List<RecipeBriefViewModel> results) {
        HashMap<String, List<RecipeBriefViewModel>> resultMap = new HashMap<>();
        resultMap.put(search, results);
        liveSearchResults.postValue(resultMap);
    }
}
