
package mytest.chefapp.network.network_models.RecipeDetailsResponse;

import com.google.gson.annotations.SerializedName;

public class Similar {

    @SerializedName("name")
    private String mName;
    @SerializedName("uuid")
    private String mUuid;

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getUuid() {
        return mUuid;
    }

    public void setUuid(String uuid) {
        mUuid = uuid;
    }

}
