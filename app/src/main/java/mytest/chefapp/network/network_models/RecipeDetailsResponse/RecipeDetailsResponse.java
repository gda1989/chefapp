
package mytest.chefapp.network.network_models.RecipeDetailsResponse;

import com.google.gson.annotations.SerializedName;

public class RecipeDetailsResponse {

    @SerializedName("recipe")
    private RecipeDetailsModel mRecipe;

    public RecipeDetailsModel getRecipe() {
        return mRecipe;
    }

    public void setRecipe(RecipeDetailsModel recipe) {
        mRecipe = recipe;
    }

}
