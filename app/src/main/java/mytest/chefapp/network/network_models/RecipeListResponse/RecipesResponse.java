
package mytest.chefapp.network.network_models.RecipeListResponse;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class RecipesResponse {

    @SerializedName("recipes")
    private List<Recipe> mRecipes;

    public List<Recipe> getRecipes() {
        return mRecipes;
    }

    public void setRecipes(List<Recipe> recipes) {
        mRecipes = recipes;
    }

}
