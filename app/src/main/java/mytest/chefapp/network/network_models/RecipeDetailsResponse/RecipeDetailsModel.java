
package mytest.chefapp.network.network_models.RecipeDetailsResponse;

import java.util.List;
import com.google.gson.annotations.SerializedName;

import mytest.chefapp.ui.models.RecipeBriefViewModel;

public class RecipeDetailsModel {

    @SerializedName("description")
    private String mDescription;
    @SerializedName("difficulty")
    private Integer mDifficulty;
    @SerializedName("images")
    private List<String> mImages;
    @SerializedName("instructions")
    private String mInstructions;
    @SerializedName("lastUpdated")
    private Long mLastUpdated;
    @SerializedName("name")
    private String mName;
    @SerializedName("similar")
    private List<Similar> mSimilar;
    @SerializedName("uuid")
    private String mUuid;

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public Integer getDifficulty() {
        return mDifficulty;
    }

    public void setDifficulty(Integer difficulty) {
        mDifficulty = difficulty;
    }

    public List<String> getImages() {
        return mImages;
    }

    public void setImages(List<String> images) {
        mImages = images;
    }

    public String getInstructions() {
        return mInstructions;
    }

    public void setInstructions(String instructions) {
        mInstructions = instructions;
    }

    public Long getLastUpdated() {
        return mLastUpdated;
    }

    public void setLastUpdated(Long lastUpdated) {
        mLastUpdated = lastUpdated;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public List<Similar> getSimilar() {
        return mSimilar;
    }

    public void setSimilar(List<Similar> similar) {
        mSimilar = similar;
    }

    public String getUuid() {
        return mUuid;
    }

    public void setUuid(String uuid) {
        mUuid = uuid;
    }

}
