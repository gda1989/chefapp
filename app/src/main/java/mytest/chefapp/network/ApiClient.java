package mytest.chefapp.network;

import mytest.chefapp.BuildConfig;

public class ApiClient {

    public static Endpoints getInstance(){
        return new ServiceGenerator().createService(Endpoints.class, BuildConfig.baseAddress);
    }

}
