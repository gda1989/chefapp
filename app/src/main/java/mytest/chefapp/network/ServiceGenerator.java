package mytest.chefapp.network;

import org.greenrobot.eventbus.EventBus;

import java.util.concurrent.TimeUnit;

import mytest.chefapp.eventbus.ConnectionEvent;
import mytest.chefapp.utils.InternetConnectionChecker;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {

    private OkHttpClient getClient() {

        ConnectivityCheckerInterceptor connectionInterceptor = new ConnectivityCheckerInterceptor() {
            @Override
            public boolean isInternetAvailable() {
                boolean available = InternetConnectionChecker.getInstance().connected();
                EventBus.getDefault().post(new ConnectionEvent(!available));
                return available;
            }

            @Override
            public void onInternetUnavailable() {
                EventBus.getDefault().post(new ConnectionEvent(true));
            }

        };

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();

        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .addInterceptor(connectionInterceptor)
                .readTimeout(90, TimeUnit.SECONDS)
                .connectTimeout(80, TimeUnit.SECONDS)
                .callTimeout(90, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true);

        builder.addInterceptor(loggingInterceptor);
        return builder.build();
    }

    <S> S createService(Class<S> serviceClass, String baseURL) {


        return new Retrofit.Builder()
                .baseUrl(baseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(ServerResponseAdapterFactory.create())
                .client(getClient())
                .build()
                .create(serviceClass);
    }


}
