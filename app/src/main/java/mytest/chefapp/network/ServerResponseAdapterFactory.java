package mytest.chefapp.network;

import androidx.annotation.NonNull;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.net.ConnectException;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import mytest.chefapp.eventbus.ConnectExceptionEvent;
import mytest.chefapp.eventbus.RetryConnectionEvent;
import retrofit2.Call;
import retrofit2.CallAdapter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

public class ServerResponseAdapterFactory extends CallAdapter.Factory {

    private ServerResponseAdapterFactory() {
    }


    static ServerResponseAdapterFactory create() {
        return new ServerResponseAdapterFactory();
    }

    @Override
    public CallAdapter<?, ?> get(@NonNull Type returnType, @NonNull Annotation[] annotations, @NonNull Retrofit retrofit) {
        RxJava2CallAdapterFactory rxJava2CallAdapterFactory = RxJava2CallAdapterFactory.create();
        CallAdapter callAdapter = rxJava2CallAdapterFactory.get(returnType, annotations, retrofit);
        if (callAdapter == null) {
            return null;
        }
        return new BaseCallAdapter(retrofit, callAdapter);
    }

    private static class BaseCallAdapter<R> implements CallAdapter<R, Object> {
        private final Retrofit retrofit;
        private CallAdapter<R, Object> callAdapter;

        private BaseCallAdapter(Retrofit retrofit, CallAdapter<R, Object> callAdapter) {
            this.retrofit = retrofit;
            this.callAdapter = callAdapter;
            EventBus.getDefault().register(this);
        }

        private PublishSubject<Boolean> subject = PublishSubject.create();

        @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
        public void onEvent(RetryConnectionEvent event) {
            subject.onNext(true);
        }

        @NotNull
        @Override
        public Type responseType() {
            return callAdapter.responseType();
        }

        @NotNull
        @Override
        public Object adapt(@NonNull Call<R> call) {
            Object response = callAdapter.adapt(call);
            return handleResponse(response);
        }

        @SuppressWarnings("unchecked")

        //retry on connection available
        private Object handleResponse(Object response) {
            return ((Single) response)
                    .doOnError(throwable -> {
                        EventBus.getDefault().post(throwable);
                    })
                    .retryWhen((Function) throwableFlowable -> subject
                            .filter(aBoolean -> aBoolean).toFlowable(BackpressureStrategy.LATEST)
                            .observeOn(Schedulers.io())
                    );

        }
    }

}
