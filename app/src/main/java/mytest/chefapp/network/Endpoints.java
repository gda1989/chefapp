package mytest.chefapp.network;

import io.reactivex.Single;
import mytest.chefapp.network.network_models.RecipeDetailsResponse.RecipeDetailsResponse;
import mytest.chefapp.network.network_models.RecipeListResponse.RecipesResponse;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface Endpoints {

    @GET("/recipes")
    Single<RecipesResponse> getRecipes();

    @GET("/recipes/{uuid}")
    Single<RecipeDetailsResponse> getRecipeDetails(
            @Path("uuid") String uuid
    );
}
