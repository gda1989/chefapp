package mytest.chefapp.eventbus;

public class ConnectionEvent {

    public boolean isHaveNetworkProblems() {
        return haveNetworkProblems;
    }

    private boolean haveNetworkProblems;

    public ConnectionEvent(boolean haveNetworkProblems) {
        this.haveNetworkProblems = haveNetworkProblems;
    }

}
