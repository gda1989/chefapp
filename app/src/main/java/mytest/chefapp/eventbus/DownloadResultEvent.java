package mytest.chefapp.eventbus;

public class DownloadResultEvent {

    public String getResult() {
        return result;
    }

    private final String result;

    public DownloadResultEvent(String result) {
        this.result = result;
    }
}
