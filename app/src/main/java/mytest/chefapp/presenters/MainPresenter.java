package mytest.chefapp.presenters;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import moxy.InjectViewState;
import moxy.MvpPresenter;
import mytest.chefapp.ChefApp;
import mytest.chefapp.R;
import mytest.chefapp.data.interactors.RecipesInteractor;
import mytest.chefapp.eventbus.ConnectExceptionEvent;
import mytest.chefapp.eventbus.DownloadResultEvent;
import mytest.chefapp.network.network_models.RecipeDetailsResponse.Similar;
import mytest.chefapp.ui.live_data.LiveDataManager;
import mytest.chefapp.ui.main.MainMvpInterface;

@SuppressLint("CheckResult")
@InjectViewState
public class MainPresenter extends MvpPresenter<MainMvpInterface> {

    @Inject
    RecipesInteractor recipesInteractor;

    @Inject
    LiveDataManager liveDataManager;

    @Override
    protected void onFirstViewAttach() {
        ChefApp.get().getComponent().inject(this);
        EventBus.getDefault().register(this);
        getViewState().showRecipesListFragment();
        getRecipesList();
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void onEvent(ConnectExceptionEvent event) {
        getViewState().onConnectException();
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void onEvent(Object object) {
        if(object instanceof Throwable)
            handleErrors((Throwable) object);
    }

    public void onRecipeClick(String uuid) {
        getRecipeDetails(uuid);
        getViewState().showRecipeInfoFragment(uuid);
    }

    private void getRecipesList() {
        recipesInteractor.getRecipes().subscribe(recipes -> liveDataManager.updateBriefs(recipes), throwable -> handleErrors(throwable)
        );
    }

    public void getRecipeDetails(String uuid) {
        recipesInteractor.getRecipeDetails(uuid).subscribe(recipeDetailsModel -> liveDataManager.addRecipeDetails(uuid, recipeDetailsModel),
                throwable -> handleErrors(throwable));
    }

    public void getSimilarRecipes(List<Similar> similars) {
        Completable.fromRunnable(() -> {
            for (Similar similar : similars) {
                getRecipeDetails(similar.getUuid());
            }
        })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
    }

    public void onImageClick(String imageLink) {
        getViewState().showBigImageFragment(imageLink);
    }

    public void onBigImageCloseClick() {
        getViewState().hideBigImageFragment();
    }

    public void performSearch(String search, int mode) {
        recipesInteractor.performSearch(search, mode).subscribe(briefViewModels -> liveDataManager.setSearchResults(search, briefViewModels),
                this::handleErrors);
    }

    private String tempImageLink;

    public void onSaveImageClick(String link) {
        tempImageLink = link;
        getViewState().askPermission();
    }

    public void saveImage() {
        Completable.fromRunnable(() -> Glide.with(ChefApp.getAppContext())
                .asBitmap()
                .load(tempImageLink)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        if (tempImageLink != null) {

                            SimpleDateFormat format = new SimpleDateFormat("hh:mm:ss-dd.MM.yyyy");
                            Calendar calendar = Calendar.getInstance();
                            String fileName = TextUtils.concat("Chef", format.format(calendar.getTime()), ".jpg").toString();

                            File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath());

                            boolean dirCreated = true;
                            if (!dir.exists()) {
                                dirCreated = dir.mkdirs();
                            }

                            if (dirCreated) {
                                File imageFile = new File(dir, fileName);
                                try {
                                    OutputStream outputStream = new FileOutputStream(imageFile);
                                    resource.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
                                    outputStream.close();
                                    sendResultBroadcast(ChefApp.getAppContext().getString(R.string.image_download_success_string));

                                    //rescan /download directory
                                    Intent galleryUpdateIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                                    galleryUpdateIntent.setData(Uri.fromFile(imageFile));
                                    ChefApp.getAppContext().sendBroadcast(galleryUpdateIntent);

                                } catch (Exception e) {
                                    sendResultBroadcast(ChefApp.getAppContext().getString(R.string.download_error_string));
                                }
                            }
                        } else {
                            sendResultBroadcast("Some error occured. Please try again.");
                        }
                    }
                }))
                .observeOn(Schedulers.newThread())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {

                });

    }

    private void sendResultBroadcast(String result){
        EventBus.getDefault().post(new DownloadResultEvent(result));
    }

    private void handleErrors(Throwable throwable) {
        if (throwable instanceof ConnectException) {
            getViewState().showGlobalError();
        } else if (throwable instanceof UnknownHostException) {
            getViewState().onUnknownHostException();
        } else {
            getViewState().showGlobalError();
        }
    }

}
