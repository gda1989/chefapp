package mytest.chefapp.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import mytest.chefapp.ChefApp;

public class InternetConnectionChecker {

    private static InternetConnectionChecker instance;


    public static InternetConnectionChecker getInstance() {
        if (instance == null)
            instance = new InternetConnectionChecker();
        return instance;
    }

    public boolean connected() {
        boolean isConnected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) ChefApp.getAppContext().
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = null;
        if (connectivityManager != null) {
            activeNetwork = connectivityManager.getActiveNetworkInfo();
        }
        try {
            if (activeNetwork != null) {
                isConnected = activeNetwork.isConnected();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        subject.onNext(isConnected);
        return isConnected;
    }

    private PublishSubject<Boolean> subject = PublishSubject.create();


}
