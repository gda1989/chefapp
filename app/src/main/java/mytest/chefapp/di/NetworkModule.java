package mytest.chefapp.di;

import dagger.Module;
import dagger.Provides;
import mytest.chefapp.data.repos.RecipesRepo;
import mytest.chefapp.data.repos.impl.RecipesRepoImpl;

@Module
public class NetworkModule {

    @Provides
    RecipesRepo getRecipesRepo(){
        return new RecipesRepoImpl();
    }

}
