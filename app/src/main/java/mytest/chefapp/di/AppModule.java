package mytest.chefapp.di;

import dagger.Module;
import dagger.Provides;
import mytest.chefapp.data.interactors.RecipesInteractor;
import mytest.chefapp.data.repos.RecipesRepo;
import mytest.chefapp.ui.live_data.LiveDataManager;

@Module
public class AppModule {

    @Provides
    LiveDataManager getLiveDataManager(){
        return LiveDataManager.getInstance();
    }

    @Provides
    RecipesInteractor getRecipesInteractor(RecipesRepo recipesRepo){
        return RecipesInteractor.getInstance(recipesRepo);
    }

}
