package mytest.chefapp.di;

import dagger.Component;
import mytest.chefapp.ChefApp;
import mytest.chefapp.data.interactors.RecipesInteractor;
import mytest.chefapp.data.repos.RecipesRepo;
import mytest.chefapp.presenters.MainPresenter;
import mytest.chefapp.ui.live_data.LiveDataManager;
import mytest.chefapp.ui.main.fragments.details.RecipeDetailsFragment;
import mytest.chefapp.ui.main.fragments.RecipesListFragment;

@Component(modules = {NetworkModule.class, AppModule.class})
public interface AppComponent {

    //repos
    RecipesRepo getRecipesRepo();

    //interactors
    RecipesInteractor getRecipesInteractor();

    //liveData
    LiveDataManager getLiveDataManager();

    //injections
    void inject(ChefApp chefApp);

    void inject(MainPresenter mainPresenter);

    void inject(RecipesListFragment recipesListFragment);

    void inject(RecipeDetailsFragment recipeDetailsFragment);

}
