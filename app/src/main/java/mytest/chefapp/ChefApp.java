package mytest.chefapp;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import org.greenrobot.eventbus.EventBus;

import mytest.chefapp.di.AppComponent;
import mytest.chefapp.di.AppModule;
import mytest.chefapp.di.DaggerAppComponent;
import mytest.chefapp.eventbus.ConnectionEvent;
import mytest.chefapp.utils.InternetConnectionChecker;

public class ChefApp extends Application {

    private static Context appContext;
    private static ChefApp instance;

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = this;
        instance = this;
        initDependencyTree();
        registerReceiver(networkBroadcastReceiver, intentFilter);
    }

    public AppComponent getComponent() {
        return component;
    }

    private AppComponent component;

    public static Context getAppContext() {
        return appContext;
    }

    public static ChefApp get() {
        return instance;
    }

    private void initDependencyTree() {
        component = buildComponent();
        component.inject(this);
    }

    protected AppComponent buildComponent() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule())
                .build();
    }

    private BroadcastReceiver networkBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction() != null && intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")) {
                sendConnectionStateEvent();
            }
        }
    };

    private void sendConnectionStateEvent() {
        if (!(InternetConnectionChecker.getInstance().connected())) {
            EventBus.getDefault().post(new ConnectionEvent(true));
        } else {
            EventBus.getDefault().post(new ConnectionEvent(false));
        }
    }

    IntentFilter intentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");


}
